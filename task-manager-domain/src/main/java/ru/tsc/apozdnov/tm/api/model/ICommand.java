package ru.tsc.apozdnov.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ICommand {

    @NotNull
    String getName();

    @NotNull
    String getDescription();

    @Nullable
    String getArgument();

}